starjava-cdf (1.0+2024.08.01-1) unstable; urgency=medium

  * Update get-orig-source to use git sparse checkout instead of svn
  * New upstream version 1.0+2024.08.01
  * Rediff patches
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Sat, 12 Oct 2024 15:56:50 +0200

starjava-cdf (1.0+2022.08.15+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0+2022.08.15+dfsg
  * Push minversion of starjava-table to 4.1.3
  * Push Standards-Version to 4.6.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 07 Oct 2022 11:42:06 +0200

starjava-cdf (1.0+2021.12.15+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Remove obsolete field Contact from debian/upstream/metadata

  [ Ole Streicher ]
  * New upstream version 1.0+2021.12.15+dfsg
  * Push Standards-Version to 4.6.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 04 Jan 2022 15:20:46 +0100

starjava-cdf (1.0+2021.01.10+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0+2021.01.10+dfsg
  * Rediff patches
  * Push Standards-Version to 4.5.1. No changes needed
  * Push dh-compat to 13
  * Add "Rules-Requires-Root: no" to d/control
  * Add versionized build-deps for starlink-table

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Jan 2021 10:53:59 +0100

starjava-cdf (1.0+2020.10.01+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + starlink-cdf-java-doc: Add Multi-Arch: foreign.

  [ Ole Streicher ]
  * New upstream version 1.0+2020.10.01+dfsg
  * Rediff patches
  * Push Standards-Version to 4.5.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 06 Nov 2020 10:35:06 +0100

starjava-cdf (1.0+2019.07.12+dfsg-1) unstable; urgency=low

  * New upstream version 1.0+2019.07.12+dfsg. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required
  * Push compat to 12. Remove d/compat
  * Add gitlab-ci for salsa

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 11:17:11 +0200

starjava-cdf (1.0+2018.11.28+dfsg-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * Push Standards-Version to 4.2.1. No changes needed.
  * New upstream version 1.0+2018.11.28+dfsg
  * Push compat to 11

 -- Ole Streicher <olebole@debian.org>  Fri, 30 Nov 2018 08:29:44 +0100

starjava-cdf (1.0+2017.01.04+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #860161)

 -- Ole Streicher <olebole@debian.org>  Sat, 22 Apr 2017 14:33:07 +0200
